(ns netdava.homebank.ofx
  (:require [bux.core :as bux :refer [round$]]
            [bux.currencies :as bc]
            [clojure.string :as cstr]
            goog.crypt goog.crypt.Md5
            [netdava.homebank.iban :as ibn]
            [netdava.homebank.utils :as u]
            ["xmlbuilder2" :as xmlb2]))

(defn md5
  "Generate md5 hash of a string"
  [s]
  (goog.crypt/byteArrayToHex
   (let [md5 (goog.crypt.Md5.)]
     (.update md5 (goog.crypt/stringToUtf8ByteArray s))
     (.digest md5))))

(defn money-formatter
  "Money formatter - for currency without symbols."
  [c]
  (let [decimal-points (:decimal-points c)
        pattern (:goog-pattern c goog.i18n.NumberFormat.Format.DECIMAL)
        opt-currency (:iso-code c)
        opt-currency-style (:goog-currency-style c goog.i18n.NumberFormat.CurrencyStyle.LOCAL)
        opt-symbols (clj->js (merge (js->clj goog.i18n.NumberFormatSymbols_ro) {"DECIMAL_SEP" "," "GROUP_SEP" "."}))
        f (goog.i18n.NumberFormat. pattern opt-currency opt-currency-style opt-symbols)]
    (.setMinimumFractionDigits f decimal-points)
    (.setMaximumFractionDigits f decimal-points)
    f))

(defn parse
  "Parse a string representing currency without simbol."
  [c value]
  (round$ c (.parse (money-formatter c) (first (re-find #"([0123456789.,]+)" value)))))

(defn parse-currency
  [curr]
  (let [RON (merge (bc/iso$ "RON"))]
    (parse RON curr)))

(defn transaction-type
  "Determine if the transaction type is credit or debit, make sure we use strings"
  [tx]
  (let [{credit :credit} tx]
    (if credit
      "CREDIT"
      "DEBIT")))

(defn is-credit
  [tx]
  (let [{credit :credit} tx]
    (if credit
      true
      false)))

(defn parse-credit
  [amount]
  (parse-currency amount))

(defn parse-debit
  [amount]
  (str "-" (parse-currency amount)))

(defn transaction-amt
  [tx]
  (let [{credit :credit
         debit :debit} tx]
    (try (if (is-credit tx)
           (parse-credit credit)
           (parse-debit debit))
         (catch js/Object e ((js/console.log e)
                             (throw (js/Error. (str "Exception parsing credit/debit " (prn-str tx)))))))))

(defn sorted-tx-details
  "Sorted transaction details in a deterministic way."
  [tx]
  (let [{title :title
         date :date
         credit :credit
         debit :debit
         bal :balance} tx]
    (str (sort [title date credit debit bal]))))

(defn unique-tx-id
  "Builds a unique transaction id based on transaction data."
  [tx]
  (md5 (sorted-tx-details tx)))

(defn ofx-datetime-fmt
  "Formats a date as 3.2.8.2 Date and Datetime - YYYYMMDD. Input is YYYY-MM-DD so we can string replace."
  [date]
  (cstr/replace date #"-" ""))

(defmulti map-transaction (fn [tx] (:title tx)))

(defmethod map-transaction "Cumparare POS"
  [tx]
  (let [{date :date
         details :details
         title :title
         {:strs [Terminal Referinta]} :parsed-details} tx
        tx-id (unique-tx-id tx)
        type (transaction-type tx)
        amt (transaction-amt tx)]
    {"TRNTYPE" type
     "DTPOSTED" (ofx-datetime-fmt date)
     "TRNAMT" amt
     "NAME" (str title " :: " Terminal)
     "FITID" tx-id
     "MEMO" details
     "REFNUM" Referinta}))

(defmethod map-transaction "Taxe si comisioane"
  [tx]
  (let [{date :date
         details :details
         title :title
         {:strs [Referinta]} :parsed-details} tx
        tx-id (unique-tx-id tx)
        type (transaction-type tx)
        amt (transaction-amt tx)]
    {"TRNTYPE" type
     "DTPOSTED" (ofx-datetime-fmt date)
     "TRNAMT" amt
     "NAME" title
     "FITID" tx-id
     "MEMO" details
     "REFNUM" Referinta}))

(defmethod map-transaction "Comision pe operatiune "
  [tx]
  (let [{date :date
         title :title} tx
        tx-id (unique-tx-id tx)
        type (transaction-type tx)
        amt (transaction-amt tx)]
    {"TRNTYPE" type
     "DTPOSTED" (ofx-datetime-fmt date)
     "TRNAMT" amt
     "NAME" title
     "FITID" tx-id}))

(defmethod map-transaction "Plata debit direct"
  [tx]
  (let [{date :date
         details :details
         title :title
         {:strs [Beneficiar Referinta]} :parsed-details} tx
        tx-id (unique-tx-id tx)
        type (transaction-type tx)
        amt (transaction-amt tx)]
    {"TRNTYPE" type
     "DTPOSTED" (ofx-datetime-fmt date)
     "TRNAMT" amt
     "NAME" (str title " :: " Beneficiar)
     "FITID" tx-id
     "MEMO" details
     "REFNUM" Referinta}))

(defmethod map-transaction "Actualizare dobanda"
  [tx]
  (let [{date :date
         details :details
         title :title} tx
        tx-id (unique-tx-id tx)
        type (transaction-type tx)
        amt (transaction-amt tx)]
    {"TRNTYPE" type
     "DTPOSTED" (ofx-datetime-fmt date)
     "TRNAMT" amt
     "NAME" title
     "FITID" tx-id
     "MEMO" details}))

(defmethod map-transaction "Incasare"
  [tx]
  (let [{date :date
         details :details
         title :title
         {:strs [Ordonator Referinta]} :parsed-details} tx
        tx-id (unique-tx-id tx)
        type (transaction-type tx)
        amt (transaction-amt tx)]
    {"TRNTYPE" type
     "DTPOSTED" (ofx-datetime-fmt date)
     "TRNAMT" amt
     "NAME" (str title " :: " Ordonator)
     "FITID" tx-id
     "MEMO" details
     "REFNUM" Referinta}))

(defmethod map-transaction "Transfer Home'Bank"
  [tx]
  (let [{date :date
         details :details
         title :title
         {:strs [Beneficiar Referinta]} :parsed-details} tx
        tx-id (unique-tx-id tx)
        type (transaction-type tx)
        amt (transaction-amt tx)]
    {"TRNTYPE" type
     "DTPOSTED" (ofx-datetime-fmt date)
     "TRNAMT" amt
     "NAME" (str title " :: " Beneficiar)
     "FITID" tx-id
     "MEMO" details
     "REFNUM" Referinta}))



; Handles the following types of transactions (+ more unknown)
; "Retragere numerar"
; "Cumparare POS corectie"
; "Schimb valutar Home'Bank"
; "Constituire depozit"
; "Constituire cont de depozit"
(defmethod map-transaction
  :default
  [tx]
  (let [{date :date
         details :details
         title :title
         {:strs [Terminal]} :parsed-details} tx
        tx-id (unique-tx-id tx)
        type (transaction-type tx)
        amt (transaction-amt tx)]
    {"TRNTYPE" type
     "DTPOSTED" (ofx-datetime-fmt date)
     "TRNAMT" amt
     "NAME" (str title " :: " Terminal)
     "FITID" tx-id
     "MEMO" details}))

(defn ledger-balance
  "Build the ledger balance LEDGERBAL element from OFX 11.4.2.2 Response <STMTRS> "
  ; https://bugs.tryton.org/issue8879?show_all_history=yes
  ; https://github.com/odoo/odoo/issues/3003
  [txs]
  (let [balance-amount (get-in txs [:balance :end])
        last-tx (last (:transactions txs))
        last-tx-date (ofx-datetime-fmt (:date last-tx))]
    {"BALAMT" balance-amount
     "DTASOF" last-tx-date}))

(defn bank-account-form
  [opts]
  (let [{iban :iban account-type :acount-type :or {account-type "CHECKING"}} opts
        parsed-iban (ibn/parse iban)
        {{bank-id :bank-id
          ;; account-id :account-id
          account-str :account-str} :bban} parsed-iban]
    {"BANKID" bank-id
     "ACCTID" account-str
     "ACCTTYPE" account-type}))

(defn transactions->ofx
  "Convert a `transactions` data structure to OFX data structure.
  You will need to convert ofx data structure to XML and write it."
  ([txs] (let [opts {:iban "RO87INGB00009999001234567"}]
           (transactions->ofx txs opts)))
  ([txs opts]
   (let [{transactions :transactions} txs
         {currency :currency :or {currency "RON"}} opts
         account-form (bank-account-form opts)
         tx-list (map map-transaction transactions)
         ledgerbal (ledger-balance txs)]
     {"OFX" {"SIGNONMSGSRSV1" {}
             "BANKMSGSRSV1" {"STMTTRNRS" {"STMTRS" {"CURDEF" currency
                                                    "BANKACCTFROM" account-form
                                                    "BANKTRANLIST" {"STMTTRN" tx-list}
                                                    "LEDGERBAL" ledgerbal}}}}})))

(defn cljs->xml
  [data]
  (let [options (clj->js {:encoding "UFT-8"})
        opt (clj->js {:prettyPrint true})
        doc (xmlb2/create options (clj->js data))]
    (.end doc opt)))

(comment
  (js/console.log (u/node-slurp "src/test/fixtures/10_18.csv.edn"))

  0)