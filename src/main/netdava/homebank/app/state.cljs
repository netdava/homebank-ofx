(ns netdava.homebank.app.state
  (:require [clojure.spec.alpha :as spec]
   [netdava.homebank.app.store :as store]
   [netdava.homebank.app.specs :as s]))

(def messages {:netdava.homebank.app.specs/iban-mod97-valid "Typo? IBAN doesn't look valid - Account Number checksum not correct "
               :netdava.homebank.app.specs/iban-length "For Romania, IBAN length must be 24 characters long"
               :netdava.homebank.app.specs/currency "Please select a currency"})

(defn get-message
  [via messages]
  (->> (reverse via)
       (some messages)))

(defn ?message
  [spec val]
  (let [error (s/spec-validate spec val)
        message (get-message error messages)]
    message))

(defn validate-homebank-iban
  [iban]
  (let [error (s/spec-validate ::s/iban-length iban)
        message (get-message error messages)]
    message))

(defn set-current-iban!
  [app-state selected-iban]
  (swap! app-state assoc :current-iban selected-iban))

(defn add-iban!
  [app-state new-iban]
  (let [{ibans :ibans
         current-currency :current-currency} @app-state
        updated-ibans (conj ibans {:iban new-iban :currency current-currency})]
    (swap! app-state assoc :ibans updated-ibans)))

(defn delete-iban
  [target ibans]
  (into [] (filter (fn [x] (not= (:iban x) target)) ibans)))

(defn remove-iban-from-state
  [iban app-state]
  (let [{ibans :ibans} @app-state]
    (swap! app-state assoc :ibans (delete-iban iban ibans))))

(defn update-local-storage
  [app-state]
  (let [{ibans :ibans} @app-state]
    (store/save-ibans ibans)))

(defn add-iban-err-message!
  [app-state error-message]
  (swap! app-state assoc-in [:homebank-form :iban :error] error-message))

(defn add-currency-err-message!
  [app-state error-message]
  (swap! app-state assoc-in [:homebank-form :currency :error] error-message))

(comment 
  (validate-homebank-iban "RO37INGB0000999906991")
  (count "RO37INGB0000999906991908")
  (spec/explain-data ::s/iban "RO37INGB000099990699190")
  (spec/spec-validate ::s/currency nil)
  
  
  (:netdava.homebank.app.specs/iban-mod97-valid messages)
  
  0)