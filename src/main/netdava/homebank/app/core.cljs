(ns netdava.homebank.app.core
  "This namespace contains your application and is the entrypoint for 'yarn start'."
  (:require [reagent.core :as r]
            [netdava.homebank.app.localstorage :as localstorage :refer [get-edn-item]]
            [netdava.homebank.app.converter :as app_converter]))

(defn read-ibans-from-local-storage 
  []
  (get-edn-item "netdava.ofx.iban-list"))

(defn ^:dev/after-load render
  "Render the toplevel component for this app."
  []
  (let [local-ibans (read-ibans-from-local-storage)
        initial-state (r/atom {:csv-file nil
                               :ibans local-ibans
                               :currencies [{:value "RON" :text "RON"}
                                            {:value "EUR" :text "EUR"}
                                            {:value "USD" :text "USD"}]
                               :remember-this-iban false
                               :current-currency nil
                               :current-iban nil
                               :homebank-form {:iban {:error nil}
                                               :currency {:error nil}}})]
    (r/render [app_converter/converter initial-state] (.getElementById js/document "app"))))

(defn ^:export main
  "Run application startup logic."
  []
  (render))

(comment 
  0)