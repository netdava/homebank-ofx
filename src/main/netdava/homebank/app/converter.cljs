(ns netdava.homebank.app.converter
  (:require ["file-saver" :as FileSaver]
            ["semantic-ui-react" :as ui]
            [reagent.core :as r]
            [clojure.spec.alpha :as spec]
            [netdava.homebank.csv-demo :refer [app-homebank-csv->json]]
            [netdava.homebank.app.store :as store]
            [netdava.homebank.app.state :as state]
            [netdava.homebank.app.specs :as s]))

(defn get-message
  [via messages]
  (->> (reverse via)
       (some messages)))

(defn delete-icon
  [app-state iban]
  (r/as-element [:div.delete-icon {:id iban
                                   :on-click (fn [_] #_:clj-kondo/ignore(do
                                                       (state/remove-iban-from-state iban app-state)
                                                       (state/update-local-storage app-state)))} "x"]))

(defn iban->option
  [app-state]
  (fn [item]
    (let [iban (:iban item)
          currency (:currency item)
          icon (delete-icon app-state iban)]
      {:value iban
       :text (str iban " " currency)
       :description icon})))

(defn ibans->options
  [ibans app-state]
  (into [] (map (iban->option app-state) ibans)))

(defn csv-file-input
  [app-state]
  (let [swap-csv-file #(swap! app-state assoc :csv-file (clj->js (-> % .-target)))]
    [:div.field
     [:input.ui.file.input {:type "file" :accept "application/csv, text/csv" 
                            :name "csv-file" :id "csv-file" 
                            :on-change swap-csv-file}]]))

(defn error-labels
  [error]
  (when error [:> ui/Label
   {:class-name "label-hidden error"
    :prompt true} error]))

(defn select-iban
  [app-state]
  (let [{ibans :ibans
         {{error-message :error} :iban} :homebank-form} @app-state
        has-error? ((complement nil?) error-message)
        options (ibans->options ibans app-state)]
    [:> ui/Form.Field
     [:label "IBAN:"]
     [:> ui/Dropdown
      {:fluid true
       :error has-error?
       :selection true
       :search true
       :allow-additions true
       :options options
       :on-add-item (fn [_ data] (let [new-iban (-> data .-value)]
                                   (state/add-iban! app-state new-iban)))
       :on-change (fn [_ data] (let [selected-iban (-> data .-value)]
                                 (state/set-current-iban! app-state selected-iban)
                                 ))
       :on-search-change (fn [_ data] (let [iban (-> data .-searchQuery)
                                            error-message (state/validate-homebank-iban iban)]
                                        (state/add-iban-err-message! app-state error-message)
                                        ))}]
     (error-labels error-message)
     ]))

(defn autofill-currency
  [app-state]
  (let [{ibans :ibans
        selected-iban :current-iban} @app-state
        iban-data (into {} (filter (fn [iban] (= (:iban iban) selected-iban)) ibans))]
    (:currency iban-data)))

(defn select-currency
  [app-state]
  (let [{options :currencies
         {{error :error} :currency} :homebank-form} @app-state
         has-error? (if (= nil error) false true)
        change-selection (fn [_ data] (let [selected-currency (-> data .-value)] #_:clj-kondo/ignore(do (swap! app-state assoc :current-currency selected-currency)
                                                                                     (swap! app-state assoc-in [:homebank-form :currency :error] (state/?message ::s/currency selected-currency)))))
        placeholder (if (nil? (autofill-currency app-state)) "Select currency ..." (autofill-currency app-state))
        default-value (if (nil? (autofill-currency app-state)) nil (autofill-currency app-state))]
    [:<>
     [:div.field
     [:label "Currency:"]
      [:> ui/Dropdown
     {:placeholder placeholder
      :default-value default-value
      :error has-error?
      :fluid true
      :selection true
      :options options
      :on-change change-selection}]
      (error-labels error)]
     ]))

;; (defn select-iban2
;;   [app-state]
;;   (let []
;;     [:> ui/Dropdown {:fluid true :search true}
;;      [:> ui/Dropdown.Menu
;;       [:> ui/Dropdown.Item {:text "item"}]]]))

(defn remember-data-checkbox
  [state]
  (let [s @state
        {remember :remember-this-iban} s
        remember-option #(swap! state assoc :remember-this-iban (not remember))]
    [:div.field {:style {:margin "0 0 1em"}}
     [:div.ui.toggle.checkbox
      [:input {:type :checkbox
               :id "remember-iban"
               :name "remember-iban"
               :checked remember
               :on-change remember-option}]
      [:label {:for "remember-iban"} "Remember IBAN"]]]))

(defn save-to-file
  [data name]
  (let [file (js/File. [data] name (clj->js {:type "text/plain;charset=utf-8"}))]
    (FileSaver/saveAs file)))

(defn convert-and-save-ofx-builder
  [file]
  (fn [ev]
    (let [result (-> ev .-target .-result)
          data (app-homebank-csv->json result)
          f-name (str (-> file .-name) ".ofx")]
      (save-to-file data f-name))))

(defn process-file
  [app-state]
  (let [s @app-state
        {csv-file :csv-file} s
        file (-> csv-file .-files (aget 0))
        reader (js/FileReader.)]
    (set! (.-onload reader) (convert-and-save-ofx-builder file))
    (when file
      (.readAsText reader file))))

(defn add-iban
  [iban-list current-iban]
  (if (nil? iban-list)
    [current-iban]
    (conj iban-list current-iban)))

(defn remember-iban
  [iban currency]
  (let [current-iban {:iban iban :currency currency}
        update-ibans-list (add-iban (store/load-ibans) current-iban)]
    (store/save-ibans update-ibans-list)))

(defn form-has-errors?
  [app-state]
  (let [{{{iban-error? :error} :iban
          {currency-error? :error} :currency} :homebank-form} @app-state]
    (and (nil? iban-error?) (nil? currency-error?))))

(defn check-for-errors
  [app-state]
  (let [{iban :current-iban
         currency :current-currency} @app-state
        iban-err-msg (if (= iban nil) nil (state/?message ::s/iban iban))
        currency-err-msg (state/?message ::s/currency currency)]
    (state/add-iban-err-message! app-state iban-err-msg)
    (state/add-currency-err-message! app-state currency-err-msg)))

(defn convert-btn
  [app-state]
  (let [{file :csv-file
         save-iban :remember-this-iban
         iban :current-iban
         currency :current-currency} @app-state
        convert-file (fn []
                       (process-file app-state)
                       (when save-iban
                         #_:clj-kondo/ignore (do (remember-iban iban currency)
                                                 (.reload js/window.location true))))
        has-file? (nil? file)]
    [:button.fluid.ui.primary.button
     {:on-click (fn [] #_:clj-kondo/ignore(do (check-for-errors app-state)
                     (when (form-has-errors? app-state) (convert-file))))
      :disabled has-file?} "Convert"]))

(defn converter
  [app-state]
  [:<>
   [:div.ui.centered.grid.container
    [:div.column {:style {:max-width 450}}
     [:h1 "Homebank-OFX"]
     [:> ui/Form
      [csv-file-input app-state]
      [select-iban app-state]
      [select-currency app-state]
      [remember-data-checkbox app-state]]
     [convert-btn app-state]]]])

(comment
  (def state (r/atom {:dropped false}))

  (let [options [{:value "RO17INGB2871827187281" :text "RO17INGB2871827187281"}
                 {:value "RO17INGB2871825645654" :text "RO17INGB2871825645654"}
                 {:value "RO17INGB2871827187286" :text "RO17INGB2871827187286"}]]
    (into [] (filter (fn [x] (not= (:value x) "RO17INGB2871827187281")) options)))

  (spec/valid? ::s/iban-length "RO37INGB0000999906991908")

  (let [errors [{:error-text "Invalid length"}]]
    [error-labels (clj->js errors)])

  (get-message (s/spec-validate ::s/form {:iban "RO37INGB0000999906991909"})
             {:netdava.homebank.app.specs/iban-mod97-valid "Not satisfy mod-97 rules." :netdava.homebank.app.specs/iban-length "Must be 24 characters" :netdava.homebank.app.specs/iban-not-empty "Must be 24 characters"})
  
  0)
