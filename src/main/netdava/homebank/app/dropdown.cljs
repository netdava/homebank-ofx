(ns netdava.homebank.app.dropdown)

(defn item-markup 
  [elem]
  [:div.item 
   {:data-value (:iban elem)} 
   (str (:iban elem) " " (:currency elem))])

(defn items-mark-up
  [items]
  (map item-markup items))

(defn dropdown-element
  [app-state]
  (let [s @app-state
        {ibans :ibans} s]
    [:div.ui.fluid.search.selection.dropdown
     [:input {:type "hidden" :name "select iban"}]
     [:div.default.text "enter iban"]
     [:i.dropdown.icon]
     [:div.menu
      (items-mark-up ibans)]]))

(comment
  (let [ibans [{:iban "RO17INGB2871827187281" :currency "RON"}
               {:iban "RO17INGB2871825645654" :currency "EUR"}
               {:iban "RO17INGB2871827187286" :currency "USD"}]
        to-markup (fn [elem] [:div.item {:data-value (:iban elem) } (str (:iban elem) " " (:currency elem))])]
    (map to-markup ibans))
  
  0)