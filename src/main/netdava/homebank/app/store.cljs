(ns netdava.homebank.app.store
  (:require [netdava.homebank.app.localstorage :as ls]))

(defn save-ibans
  [ibans]
  (ls/set-item! "netdava.ofx.iban-list" ibans))

(defn remove-iban
  [])

(defn load-ibans
  []
  (ls/get-edn-item "netdava.ofx.iban-list"))