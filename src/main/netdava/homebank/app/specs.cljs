(ns netdava.homebank.app.specs
  (:require [clojure.spec.alpha :as s]
            [netdava.homebank.app.validate-iban :as v]))

(s/def ::iban-length #(= 24 (count %))) ;; check for Romanian IBAN length of 24 characters
(s/def ::iban-mod97-valid #(v/validate-iban %)) ;; converting IBAN into an integer and performing a basic mod-97 operation on it -> returns true if valid, false otherwise
(s/def ::iban (s/and ::iban-length ::iban-mod97-valid))
(s/def ::currency not-empty)

(s/def ::form (s/keys :req-un [::iban ::currency]))

(defn ?spec-problems
  "Return nil if pass."
  [spec value]
  (-> (s/explain-data spec value)
      :cljs.spec.alpha/problems))

(defn spec-validate
  "Check value by spec.
  If validate return nil,
  otherwise return a reason: the :via value of spec problem."
  [spec value]
  (->> (?spec-problems spec value)
       (first)
       :via))

(comment
  (spec-validate ::form {:iban "RO59RNCBX899991601"})
  (s/explain-data ::iban {:iban "RO59RNCBX899991601"})
  (s/spec-validate ::iban "RO59RNCBX8999916")
  (spec-validate ::iban "RO59RNCBX8999916")
  (s/valid? ::currency nil)
  (spec-validate ::form {:iban "RO37INGB00009999069919" :currency nil})
 0)