(ns netdava.homebank.app.validate-iban
  (:require [clojure.string :as s]))

(defn move-first-4
  "Move the four initial characters to the end of the string"
  [iban]
  (let [rest (subs iban 4)
        first-4 (subs iban 0 4)]
   (str rest first-4)))

(defn iban->string-with-nums
  "Replace the letters in the string with digits, expanding the string as necessary, such that A or a = 10, B or b = 11, and Z or z = 35. Each alphabetic character is therefore replaced by 2 digits"
  [iban]
  (let [en-alphabet ["A" "B" "C" "D" "E" "F" "G" "H" "I" "J" "K" "L" "M" "N" "O" "P" "Q" "R" "S" "T" "U" "V" "W" "X" "Y" "Z"]
        letter-not-in-alphabet? #(= (.indexOf en-alphabet %) -1)
        index (fn [letter] (if (letter-not-in-alphabet? letter) letter (+ 10 (.indexOf en-alphabet letter))))
        move-iban-initials (move-first-4 iban)]
    (s/join (map index move-iban-initials))))


(defn mod-97-recursive
  [reminder string]
  (let [first7-str (subs string 0 7)
        trimmed-string (s/replace string first7-str "")
        transform-in-string (fn [a b] (str a b))
        construct-num (js/parseInt (transform-in-string reminder first7-str))
        new-result (mod construct-num 97)]
    (if (= (count trimmed-string) 0) 
      new-result
     (mod-97-recursive new-result trimmed-string))
    ))

(defn modulo-on-IBAN
  "Interpret the string as a decimal integer and compute the remainder of that number on division by 97; If the remainder is 1, the check digit test is passed and the IBAN might be valid.
   Steps to calculate:
   1. Starting from the leftmost digit of an integer D, construct a number N using the first 9 digits;
   2. Calculate N mod 97;
   3. Construct a new 9-digit N by concatenating above result (step 2) with the next 7 digits of D. If there are fewer than 7 digits remaining in D but at least one, then construct a new N, which will have less than 9 digits, from the above result (step 2) followed by the remaining digits of D;
   4. Repeat steps 2–3 until all the digits of D have been processed."
  [iban]
  (let [parsed-iban (iban->string-with-nums iban)
        first9-str (subs parsed-iban 0 9) ;; string
        trimmed-string (s/replace parsed-iban first9-str "") ;;string
        first9-num (js/parseInt first9-str) ;;number
        reminder (mod first9-num 97)] ;;number
    (mod-97-recursive reminder trimmed-string)))

(defn validate-iban
  "An IBAN is validated by converting it into an integer and performing a basic mod-97 operation (as described in ISO 7064) on it.
   Source: https://en.wikipedia.org/wiki/International_Bank_Account_Number#Validating_the_IBAN"
  [iban]
  (if (= (modulo-on-IBAN iban) 1) true false))

(comment
  (def i "RO37INGB0000999906991908")
  
  (iban->string-with-nums i)
  (move-first-4 i)
  (iban->string-with-nums (move-first-4 i))
  (modulo-on-IBAN (iban->string-with-nums (move-first-4 i)))
  (iban->string-with-nums (move-first-4 i))
  (modulo-on-IBAN "RO37INGB0000999906991908")
  (+ 2 4)
  (validate-iban "RO37INGB0000999906991908")
  (validate-iban "RO37INGB0000999906991908")
  0)