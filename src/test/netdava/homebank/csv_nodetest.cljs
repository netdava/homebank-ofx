(ns netdava.homebank.csv-nodetest
  (:require ["fs" :as fs]
            [cljs.test :refer (deftest is)]
            [netdava.homebank.csv :as h]
            [netdava.homebank.csvparse :as csvp]))

;; These tests will run in node.js
;; Jack in in `:test` build and choose `node-repl`

(defn node-slurp [path]
  (.readFileSync fs path "utf8"))

(def csv-10 (node-slurp "src/test/fixtures/10_18.csv"))

(deftest data-loads-ok-from-file
  (let [csv-lines (get (csvp/parse-csv csv-10) "data")
        csv-line-2 (second csv-lines)]
    (is (= csv-line-2 ["01 octombrie 2018" "" "Plata debit direct" "" "" "15,00" "" ""]))))

(deftest csv-10-account-balance-is-parsed
  (let [csv-lines (get (csvp/parse-csv csv-10) "data")
        txs (h/process-statement-data csv-lines)
        {balance :balance} txs]
    (is (= balance {:start "128,07" :end "-2,23"}))))

(deftest csv-10-account-transactions-are-parsed
  (let [csv-lines (get (csvp/parse-csv csv-10) "data")
        txs (h/process-statement-data csv-lines)
        {balance :balance} txs]
    ;; (js/console.log (clj->js txs))
    (is (= balance {:start "128,07" :end "-2,23"}))))



(comment

  (js/console.log (clj->js (get (csvp/parse-csv csv-10) "data")))
  (node-slurp "src/test/fixtures/10_18.csv")

  0)