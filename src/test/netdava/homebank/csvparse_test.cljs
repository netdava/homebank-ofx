(ns netdava.homebank.csvparse-test
  (:require [cljs.test :refer (deftest is)]
            [netdava.homebank.csvparse :as csvp]))

(def csv-fixture (str ",Data,,Detalii tranzactie,,,Debit,Credit\n01 octombrie 2018,,Plata debit direct,,,\"15,00\",,\n"))

(deftest parse-csv-parse-integration-test
  (let [expected {"data" [["" "Data" "" "Detalii tranzactie" "" "" "Debit" "Credit"]
                          ["01 octombrie 2018" "" "Plata debit direct" "" "" "15,00" "" ""]
                          [""]]
                  "errors" []
                  "meta" {"delimiter" "," "linebreak" "\n" "aborted" false "truncated" false "cursor" 91}}]
    (is (= (csvp/parse-csv csv-fixture) expected))))

(comment
  
  (js/console.log csv-fixture)
  0)
