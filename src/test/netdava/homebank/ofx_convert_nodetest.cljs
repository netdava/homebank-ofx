(ns netdava.homebank.ofx-convert-nodetest
  (:require [cljs.test :refer (deftest is)]
            [clojure.edn :as edn]
            [netdava.homebank.ofx :as ofx]
            [netdava.homebank.utils :as u]))

;;
;; Test ofx conversion with sample data.
;; Load transactions from file, convert, write ofx
;;

(defn read-edn [f] (edn/read-string (u/node-slurp f)))

(deftest convert-edn-10-to-ofx
  (let [edn-10 (read-edn "src/test/fixtures/10_18.csv.edn")
        opts {:iban "RO87INGB00009999001234567"}
        ofx (ofx/transactions->ofx edn-10 opts)]
    (is (= (get-in ofx ["OFX" "BANKMSGSRSV1" "STMTTRNRS" "STMTRS" "LEDGERBAL" "BALAMT"]) "-2,23"))))
