(ns netdava.homebank.ofx-test
  (:require [cljs.test :refer (deftest is)]
            [netdava.homebank.ofx :as ofx]))

(deftest a-failing-test
  (is (= 1 1)))


(deftest md5-test
  (is (= (ofx/md5 "apple") "1f3870be274f6c49b3e31a0c6728957f")))

(deftest parse-currency-test1
  (is (= (ofx/parse-currency "3.500,00") "3500.00")))
(deftest money-formatter-test2
  (is (= (ofx/parse-currency "17,98") "17.98")))

(deftest transaction-type-credit-test
  (let [tx-credit {:tx-number 1 :date "2018-10-01" :title "Plata debit direct" :original-data () :parsed-details {} :credit "15,00"}]
    (is (= (ofx/transaction-type tx-credit) "CREDIT"))))

(deftest transaction-type-debit-test
  (let [tx-debit {:tx-number 1 :date "2018-10-01" :title "Plata debit direct" :original-data () :parsed-details {} :debit "15,00"}]
    (is (= (ofx/transaction-type tx-debit) "DEBIT"))))

(deftest is-credit-test1
  (let [tx-credit {:tx-number 1 :date "2018-10-01" :title "Plata debit direct" :original-data () :parsed-details {} :credit "15,00"}]
    (is (= (ofx/is-credit tx-credit) true))))

(deftest is-credit-test2
  (let [tx-debit {:tx-number 1 :date "2018-10-01" :title "Plata debit direct" :original-data () :parsed-details {} :debit "15,00"}]
    (is (= (ofx/is-credit tx-debit) false))))

(deftest parse-credit-test
  (is (= (ofx/parse-credit "15") "15.00")))

(deftest parse-debit-test
  (is (= (ofx/parse-debit "17,67") "-17.67")))

(deftest transaction-amt-test1
  (let [tx-credit {:tx-number 1 :date "2018-10-01" :title "Plata debit direct" :original-data () :parsed-details {} :credit "15,00"}]
    (is (= (ofx/transaction-amt tx-credit) "15.00"))))

(deftest transaction-amt-test2
  (let [tx-debit {:tx-number 1 :date "2018-10-01" :title "Plata debit direct" :original-data () :parsed-details {} :debit "15,00"}]
    (is (= (ofx/transaction-amt tx-debit) "-15.00"))))

(def tx {:title "Tranzactie 1"
         :date "2020-09-21"
         :credit "999"
         :debit "0"
         :balance "1340"})

(deftest sorted-tx-details-test
  (is (= (ofx/sorted-tx-details tx) "(\"0\" \"1340\" \"2020-09-21\" \"999\" \"Tranzactie 1\")")))

(deftest unique-tx-id
  (is (= (ofx/unique-tx-id tx) "c2c76e20938b09f74b0c6c8bde19ba5e")))

(comment
  (defn ccc
    []
    (let [tx-debit {:tx-number 1 :date "2018-10-01" :title "Plata debit direct" :original-data () :parsed-details {} :debit "15,00"}]
      (is (= (ofx/transaction-amt tx-debit) "-15.00")))
    )
  0)