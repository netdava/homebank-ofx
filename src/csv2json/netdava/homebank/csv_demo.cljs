(ns netdava.homebank.csv-demo
  (:require [netdava.homebank.csv :as c]
            [netdava.homebank.csvparse :as csvp]
            [netdava.homebank.utils :as u]
            [netdava.homebank.ofx :as ofx]))

;; Sample nodejs script to convert csv -> json, edn, ofx files
;;
;; npx shadow-cljs compile parse-demo
;; node out/parse-demo.js
;;

(defn homebank-csv->json
  [{fin :fin fout :fout fout-edn :fout-edn fout-xml :fout-xml}]
  (let [raw (u/node-slurp fin)
        csv-lines (get (csvp/parse-csv raw) "data")
        txns (c/process-statement-data csv-lines)
        txns-json (js/JSON.stringify (clj->js txns) nil 2)
        ofx-txns (ofx/transactions->ofx txns)]
    (js/console.log (str "Processing " fin " -> " fout))
    (u/node-spit fout txns-json)
    (js/console.log (str "Processing " fin " -> " fout-edn))
    (u/node-spit fout-edn (prn-str txns))
    (js/console.log (str "Processing " fin " -> " fout-xml))
    (u/node-spit fout-xml (ofx/cljs->xml ofx-txns))))

(defn app-homebank-csv->json
  [raw]
  (let [csv-lines (get (csvp/parse-csv raw) "data")
        txns (c/process-statement-data csv-lines)
        txns-json (js/JSON.stringify (clj->js txns) nil 2)
        ofx-txns (ofx/transactions->ofx txns)]
     (ofx/cljs->xml ofx-txns)
    ))

(defn ff-names
  [f]
  {:fin (str "src/test/fixtures/" f)
   :fout (str "src/test/fixtures/" f ".json")
   :fout-edn (str "src/test/fixtures/" f ".edn")
   :fout-xml (str "src/test/fixtures/" f ".ofx")})


(defn main
  #_:clj-kondo/ignore
  [& cli-args]
  (let [files ["10_18.csv" "11_18.csv" "12_18.csv"]
        ff (map ff-names files)]
    (println ff)
    (dorun (map homebank-csv->json ff ))))


(comment

  (ff-names "kitty")
  (let [files ["10_18.csv" "11_18.csv" "12_18.csv"]
        ff (map ff-names files)]
    ff)
  0)
