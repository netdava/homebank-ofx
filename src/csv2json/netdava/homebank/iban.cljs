(ns netdava.homebank.iban
  (:require [clojure.string :as str]))


(defn parse
  "Very simplistic parse of IBAN code. Not following spec."
  [iban]
; http://rosettacode.org/wiki/IBAN
; The IBAN consists of up to 34 alphanumeric characters:
;               first the two-letter ISO 3166-1 alpha-2 country code,
;               then two check digits, and
;               finally a country-specific Basic Bank Account Number (BBAN).
  (let [country (subs iban 0 2)
        check-digits (subs iban 2 4)
        bban (subs iban 4)
        bank (subs bban 0 4)
        bank-ids (map #(.charCodeAt % 0) bank)
        bank-id (str/join bank-ids)
        account-str (subs bban 4)
        account-id (subs bban 12)]
    {:country country
     :check-digits check-digits
     :bban {:full bban
            :bank bank
            :bank-id bank-id
            :account-str account-str
            :account-id account-id}}))


(comment

  (str/join (take 2 "RO87INGB0000999905653475"))

  ;{:country "RO", :check-digits "87", :bban {:full "INGB0000999905653475", :bank "INGB", :bank-id "73787166", :account-str "0000999905653475", :account-id "05653475"}}
  (parse "RO87INGB0000999905653475")

  (nthrest "RO87INGB0000999905653475" 2)
  (drop 4 "RO87INGB0000999905653475")

  0)