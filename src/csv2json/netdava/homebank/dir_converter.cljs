(ns netdava.homebank.dir-converter
  (:require ["fs" :as fs]
            [clojure.string :as str]
            [netdava.homebank.csv :as c]
            [netdava.homebank.csvparse :as csvp]
            [netdava.homebank.utils :as u]
            [netdava.homebank.ofx :as ofx]
            ["path" :as path]))

;; Sample nodejs script to convert a directory containing csv bank statements to ofx
;;
;; npx shadow-cljs compile dir-converter
;; node out/dir-converter.js
;;

(defn process-homebank-csv
  [file-names converter-opts]
  (let [{fin :fin fout :fout fout-edn :fout-edn fout-xml :fout-xml} file-names
        raw (u/node-slurp fin)
        csv-lines (get (csvp/parse-csv raw) "data")
        txns (c/process-statement-data csv-lines)
        txns-json (js/JSON.stringify (clj->js txns) nil 2)
        ofx-txns (ofx/transactions->ofx txns converter-opts)]
    (js/console.log (str "Processing " fin " -> " fout))
    (u/node-spit fout txns-json)
    (js/console.log (str "Processing " fin " -> " fout-edn))
    (u/node-spit fout-edn (prn-str txns))
    (js/console.log (str "Processing " fin " -> " fout-xml))
    (u/node-spit fout-xml (ofx/cljs->xml ofx-txns))))

(defn homebank-csv->many
  [dir-record]
  (let [{converter-opts :converter-opts file-names :file-names} dir-record]
    (dorun (map #(process-homebank-csv % converter-opts) file-names))))

(defn ff-names
  [f]
  {:fin f
   :fout (str f ".json")
   :fout-edn (str f ".edn")
   :fout-xml (str f ".ofx")})

(defn read-dir-files
  [dir]
  (map #(path/join dir %) (js->clj (.readdirSync fs dir))))

(defn is-csv?
  [f]
  (str/ends-with? (str/lower-case f) ".csv"))

(defn read-files
  "Take a list of directory records and build a list of csv files to be processed."
  [dir-record]
  (let [{d :dir} dir-record
        files (read-dir-files d)
        csv-files (filter is-csv? files)
        file-names (map ff-names csv-files)]
    (assoc dir-record :files csv-files :file-names file-names)))

(defn main
  #_:clj-kondo/ignore
  [& cli-args]
  (let [dirs [{:dir "/home/ieugen/NetdavaCloud/Finanțe/RO12INGB0000999907807643_EUR"
               :converter-opts {:iban "RO12INGB0000999907807643"
                                :currency "EUR"}}
              {:dir "/home/ieugen/NetdavaCloud/Finanțe/RO13INGB0000999910047646_RON"
               :converter-opts {:iban "RO13INGB0000999910047646"
                                :currency "RON"}}
              {:dir "/home/ieugen/NetdavaCloud/Finanțe/RO56INGB0000999910047648_RON"
               :converter-opts {:iban "RO56INGB0000999910047648"
                                :currency "RON"}}
              {:dir "/home/ieugen/NetdavaCloud/Finanțe/RO87INGB0000999905653475_RON"
               :converter-opts {:iban "RO87INGB0000999905653475"
                                :currency "RON"}}
              {:dir "/home/ieugen/NetdavaCloud/Finanțe/RO84INGB0000999909939086_EUR"
               :converter-opts {:iban "RO84INGB0000999909939086"
                                :currency "EUR"}}]
        dirs2 [{:dir "/home/ieugen/Documente/Finanțe/Extrase de cont/RO33INGB0000999909572374_RON"
                :converter-opts {:iban "RO33INGB0000999909572374"
                                 :currency "RON"}}
               {:dir "/home/ieugen/Documente/Finanțe/Extrase de cont/RO66INGB0000999909991596_RON"
                :converter-opts {:iban "RO66INGB0000999909991596"
                                 :currency "RON"}}
               {:dir "/home/ieugen/Documente/Finanțe/Extrase de cont/RO69INGB0000999909950317_RON"
                :converter-opts {:iban "RO69INGB0000999909950317"
                                 :currency "RON"}}
               {:dir "/home/ieugen/Documente/Finanțe/Extrase de cont/RO79INGB0000999905446427_RON"
                :converter-opts {:iban "RO79INGB0000999905446427"
                                 :currency "RON"}}
               {:dir "/home/ieugen/Documente/Finanțe/Extrase de cont/RO80INGB0000999909950313_RON"
                :converter-opts {:iban "RO80INGB0000999909950313"
                                 :currency "RON"}}]
        dirs-with-files (map read-files dirs)]
    (println dirs-with-files)
    (dorun (map homebank-csv->many dirs-with-files))))

(comment

  (ff-names "kitty")
  (let [files ["10_18.csv" "11_18.csv" "12_18.csv"]
        ff (map ff-names files)]
    ff)

  (let [dirs [{:dir "/home/ieugen/NetdavaCloud/Finanțe/RO12INGB0000999907807643_EUR"
               :converter-opts {:iban "RO12INGB0000999907807643"
                                :currency "EUR"}}
              {:dir "/home/ieugen/NetdavaCloud/Finanțe/RO13INGB0000999910047646_RON"
               :converter-opts {:iban "RO13INGB0000999910047646"
                                :currency "RON"}}
              {:dir "/home/ieugen/NetdavaCloud/Finanțe/RO56INGB0000999910047648_RON"
               :converter-opts {:iban "RO56INGB0000999910047648"
                                :currency "RON"}}
              {:dir "/home/ieugen/NetdavaCloud/Finanțe/RO87INGB0000999905653475_RON"
               :converter-opts {:iban "RO87INGB0000999905653475"
                                :currency "RON"}}
              {:dir "/home/ieugen/NetdavaCloud/Finanțe/RO84INGB0000999909939086_EUR"
               :converter-opts {:iban "RO84INGB0000999909939086"
                                :currency "EUR"}}]
        files (map read-files dirs)]
    (println files))

  (let [files (read-dir-files "/home/ieugen/NetdavaCloud/Finanțe/RO12INGB0000999907807643_EUR")
        csv-files (filter is-csv? files)
        separator (str/join (take 20 (repeat "*")))
        file-names (map ff-names csv-files)]
    (println files)
    (println separator)
    (println csv-files)
    (println separator)
    (println file-names))




  0)
