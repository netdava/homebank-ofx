(ns netdava.homebank.csvdate
  (:require ["date-fns/formatISO" :as dfns-formatISO]
            ["date-fns/locale/ro" :as dfns-ro]
            ["date-fns/parse" :as dfns-parse]))

(defn csv-date-parse-unsafe
  "Parse a date from the CSV. Will throw exception if date is not valid."
  [date-str]
  (let [parse-opts (clj->js {:locale (clj->js dfns-ro)})]
    (dfns-parse date-str "dd MMMM yyyy" (js/Date.) parse-opts)))

(defn format-date
  [date]
  (let [format-iso-opts (clj->js {:representation "date"})]
    (dfns-formatISO date format-iso-opts)))

(defn csv-date-to-iso-date
  "Return ISO formatted yyyy-mm-dd date or nil if not a date/error"
  [date-str]
  (try (let [parsed-date (csv-date-parse-unsafe date-str)]
         (format-date parsed-date))
       (catch js/Object e nil)))


(comment
  
  (js/console.log dfns-parse)
  (csv-date-parse-unsafe "09 octombrie 2018")
  
  (format-date (csv-date-parse-unsafe "09 octombrie 2018"))
  
  (csv-date-to-iso-date "09 octombrie 2018")
  
  0)