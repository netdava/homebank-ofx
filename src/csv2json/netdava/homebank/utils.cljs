(ns netdava.homebank.utils
  (:require ["fs" :as fs]))

; Test helpers

(defn node-slurp
  "Read data from file `path`."
  [path]
  (.readFileSync fs path "utf8"))

(defn node-spit
  "Write data `content` to file `f`."
  [f content]
  (.writeFileSync fs f content "utf8"))