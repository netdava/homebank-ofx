(ns netdava.homebank.csvparse
  (:require ["papaparse" :as Papa]))

(defn parse-csv
  "Parses CSV string to an array of transactions"
  [csv-data]
  (js->clj (Papa/parse csv-data)))
