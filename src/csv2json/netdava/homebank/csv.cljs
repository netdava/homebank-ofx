(ns netdava.homebank.csv
  (:require [clojure.string :as str]
            [netdava.homebank.csvdate :as csvdate]))

(defn line-is-transaction-start?
  "Check if line is a transaction fisrt line.
   Transaction first lines start with a localized date"
  [csv-line]
  (let [date (csvdate/csv-date-to-iso-date (first csv-line))]
    (if date true false)))

(defn line-is-transaction-other?
  "Check if the line is part of a transaction but not the first line."
  [csv-line]
  (let [v1 (first csv-line)
        v2 (nth csv-line 1 "missing")
        v3 (nth csv-line 2 "missing")]
    (and (= "" v1 v2) (not= "" v3))))

(defn line-is-balance-start?
  "Check if line is account initial balance line."
  [csv-line]
  (str/starts-with? (str (first csv-line)) "Sold initial"))

(defn line-is-balance-end?
  "Check if line is account final balance line."
  [csv-line]
  (str/starts-with? (str (first csv-line)) "Sold final"))

(defn enhance-csv-lines
  [csv-lines]
  (let [tx-count (atom 0)]
    (for [csv-line csv-lines :let [transaction-start? (line-is-transaction-start? csv-line)
                                   transaction-other? (line-is-transaction-other? csv-line)
                                   balance-start? (line-is-balance-start? csv-line)
                                   balance-end? (line-is-balance-end? csv-line)
                                   is-transaction? (or transaction-start? transaction-other?)
                                   tx-number (if transaction-start? (swap! tx-count inc) @tx-count)]]
      {:is-transaction is-transaction?
       :tx-start transaction-start?
       :tx-number tx-number
       :balance-start balance-start?
       :balance-end balance-end?
       :data csv-line})))

(defn parse-details
  [lines]
  (let [details (map #(nth % 2) lines)
        has-semi-colon? #(> (.indexOf % ":") -1)
        with-semicolon (keep #(when (has-semi-colon? %) %) details)
        without-semicolon (remove has-semi-colon? details)
        parsed (into {} (map #(str/split % #":" 2) with-semicolon))]
    (if (empty? without-semicolon)
      parsed
      (update parsed "Detalii" #(str % " | " (str/join "," without-semicolon))))))

(defn make-transaction
  "Takes a transaction number and the lines and returns a transaction object"
  [[tx-number tx-lines]]
  (let [original-data (map #(:data %) tx-lines)
        title-line (first original-data)
        other-lines (rest original-data)
        date (csvdate/csv-date-to-iso-date (first title-line))
        title (nth title-line 2)
        debit (nth title-line 5)
        credit-index (if (contains? #{"Constituire cont de depozit"} title ) 6 7)
        credit (nth title-line credit-index)
        is-debit? (not= "" debit)
        parsed-details (parse-details other-lines)
        tx {:tx-number tx-number
            :date date
            :title title
            :original-data original-data
            :parsed-details parsed-details}]
    (if is-debit?
      (assoc tx :debit debit)
      (assoc tx :credit credit))))

(defn make-transactions
  "A transaction line starts with our 'meta-data' then the values from CSV file follow."
  [lines]
  (let [tx-lines (filter #(:is-transaction %) lines)
        tx-grouped (group-by :tx-number tx-lines)]
    (map make-transaction tx-grouped)))

(defn process-statement-data
  "Process lines from CSV bank statement to a map."
  [lines]
  (let [processed-csv-lines (enhance-csv-lines lines)
        transactions (make-transactions processed-csv-lines)
        balance-start-line (first (filter #(:balance-start %) processed-csv-lines))
        balance-end-line (first (filter #(:balance-end %) processed-csv-lines))]
    {:transactions transactions
     :balance {:start (nth (:data balance-start-line) 2)
               :end (nth (:data balance-end-line) 2)}}))

(defn generate-exports
  "Generates exports for usage in JavaScript."
  ;; See https://shadow-cljs.github.io/docs/UsersGuide.html#_dynamic_exports
  []
  #js {})


(comment

  (let [fixture [["1" "2" "" "" "" "" "" "" ""]
                 ["31 octombrie" "" "" "" "" ""]
                 ["Sold initial:" "" "128,07" ""]
                 ["Sold final " "" "-2,33" ""]
                 ["09 octombrie 2018" "" "Cumparare POS" "" "" "" "" ""]
                 ["" "" "Nr. card" "aaa" "" ""]
                 []]
        which-balance-start? (map line-is-balance-start? fixture)
        which-balance-end? (map line-is-balance-end? fixture)
        which-first-line? (map line-is-transaction-start? fixture)
        which-other-line? (map line-is-transaction-other? fixture)
        processed-lines (enhance-csv-lines fixture)]
    ;; (js/console.log (clj->js (first (filter #(:balance-start (first %))
    ;; processed-lines))))
    ;; (js/console.log (clj->js processed-lines))
    (js/console.log (clj->js (make-transactions processed-lines)))
    (js/console.log (clj->js which-balance-start?))
    (js/console.log (clj->js which-balance-end?))
    (js/console.log (clj->js which-first-line?))
    (js/console.log (clj->js which-other-line?)))


  (let [details ["Beneficiar:ASOCIATIA PENTRU RELATII"
                 "In contul:8201228910"
                 "Banca:INGB CENTRALA"
                 "Detalii:dd0127012245"
                 "006A000000VVlQc"
                 "Referinta:237044551"]
        has-semi-colon? #(> (.indexOf % ":") -1)
        with-semicolon (keep #(when (has-semi-colon? %) %) details)
        without-semicolon (remove has-semi-colon? details)
        parsed (map #(str/split % #":" 2) with-semicolon)]
    (into {} parsed))

  (str (first nil))
  (.indexOf "BancaINGB CENTRALA" ":")

  (str/starts-with? (str (first [])) "Sold final")

  (line-is-transaction-other? [])

  (line-is-transaction-start? ["09 octombrie 2018" "" "Cumparare POS"])
  (contains? #{"Constituire cont de depozit" "Incasare"} "Incasare")

  0)
