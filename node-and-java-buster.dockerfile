# Image with nodejs and jre to build the project faster
#
# docker build -f node-and-java-buster.dockerfile -t ieugen/node-and-java:12-buster-default-jre public
# docker push ieugen/node-and-java:12-buster-default-jre
#
FROM node:12-buster
RUN apt-get update && apt-get install -y default-jre && apt-get clean

