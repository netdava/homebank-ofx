module.exports = function (config) {
  config.set({
    browsers: ["ChromeHeadless"],
    // browsers: ['Chrome'],
    // The directory where the output file lives
    basePath: "out",
    // The file itself
    files: ["ci.js"],
    frameworks: ["cljs-test"],
    plugins: ["karma-cljs-test", "karma-chrome-launcher", "karma-summary-reporter", "karma-junit-reporter"],
    reporters: ["progress", "summary", "junit"],
    summaryReporter: {
      // 'failed', 'skipped' or 'all'
      show: "failed",
      // Limit the spec label to this length
      specLength: 50,
      // Show an 'all' column as a summary
      overviewColumn: true,
    },
    // the default configuration
    junitReporter: {
      outputDir: "test-reports", // results will be saved as $outputDir/$browserName.xml
      outputFile: undefined, // if included, results will be saved as $outputDir/$browserName/$outputFile
      suite: "", // suite will become the package name attribute in xml testsuite element
      useBrowserName: true, // add browser name to report and classes names
      nameFormatter: undefined, // function (browser, result) to customize the name attribute in xml testcase element
      classNameFormatter: undefined, // function (browser, result) to customize the classname attribute in xml testcase element
      properties: {}, // key value pair of properties to add to the <properties> section of the report
      xmlVersion: null, // use '1' if reporting to be per SonarQube 6.2 XML format
    },
    colors: true,
    logLevel: config.LOG_INFO,
    client: {
      args: ["shadow.test.karma.init"],
    },
    singleRun: true
  });
};
