# Image with nodejs and jre to build the project faster
#
# docker build -f node-and-java-alpine.dockerfile -t ieugen/node-and-java:12-alpine-jre-11 public
# docker push ieugen/node-and-java:12-alpine-jre-11
#
FROM node:12-alpine
RUN apk add openjdk11-jre

